#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.3"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"


import MFE
import sys
import distutils.spawn 
import matplotlib
import matplotlib.pyplot as plt
from optparse import *
from pyCRAC.Methods import getfilename
from pyCRAC.Parsers import GTF2


def main():		   
	parser = OptionParser(usage="Calculates Minimal Free Energies (MFEs) for your hybrids\n%prog [options] -f filename -o outfile", version="%s" % __version__)
	files = OptionGroup(parser, "File input options")
	files.add_option("-f", "--input_file", dest="inputfile",help="Provide the path and name of the .ua.hyb file.", metavar="FILE",default=None)
	files.add_option("-o","--output_file",dest="outputfile",help="Provide a name for an output file. By default the program writes to the standard output.",default=None)
	other = OptionGroup(parser, "Other input options")
	other.add_option("-t","--temp",dest="temp",metavar="37",type="int",help="Select the folding temperature. Default= 37C",default=37)
	other.add_option("--hist",dest="hist",action="store_true",help=SUPPRESS_HELP,default=False)
	parser.add_option_group(files)
	parser.add_option_group(other)
	(options, args) = parser.parse_args()
	
	if not distutils.spawn.find_executable("RNAcofold"):
		sys.stderr.write("\npyCalculateHybMFEs requires RNAcofold, which is not installed on this machine. Please download it from http://www.tbi.univie.ac.at/RNA/")
		exit()
	
	freeenergies = MFE.CalcChimeraMFE()
	dGs = list()	

	datain	= sys.stdin
	dataout = sys.stdout
	if options.inputfile: 
		datain = open(options.inputfile,"r")
	if options.outputfile:
		dataout = open(options.outputfile,"w")
	
	for line in datain:
		Fld = line.strip().split()
		sequence = Fld[1]
		seqfirststart,seqfirstend = int(Fld[4])-1,int(Fld[5])
		#print seqfirststart,seqfirstend
		seqsecondstart,seqsecondend = int(Fld[11])-1,int(Fld[12])
		firstseq = sequence[seqfirststart:seqfirstend]
		secondseq = sequence[seqsecondstart:seqsecondend]
		freeenergies.calculateMFE(firstseq,secondseq,temperature=options.temp)
		dG = freeenergies.minimalFreeEnergy()
		dataout.write("%s\t%s\n" % (line.strip(),dG))
		dGs.append(dG)

	if options.hist:
		hist,bins,obj = plt.hist(dGs,bins=100)
		width = 0.7*(bins[1]-bins[0])
		center = (bins[:-1]+bins[1:])/2
		plt.bar(center,hist,align='center',width=width)
		plt.savefig("%s_folding_engergies_histogram.pdf" % getfilename(options.inputfile))

if __name__ == "__main__":
	main()