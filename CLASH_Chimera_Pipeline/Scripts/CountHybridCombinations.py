#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.2"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"


import sys
import os
from collections import defaultdict
from pyCRAC.Methods import sortbyvalue
from optparse import *

def main():		   
	parser = OptionParser(usage="Finds the gene names that map to your hybrids\n%prog [options] -f filename -o outfile", version="%s" % __version__)
	parser.add_option("-f", "--input_file", dest="inputfile",help="Provide the path and name of the .ua.hyb file.", metavar="FILE",default=None)
	parser.add_option("-o", "--output_file", dest="outputfile",help="Provide an output file name. Default is standard output.", metavar="FILE",default=None)
	(options, args) = parser.parse_args()

	### setting the input and output streams
	datain	= sys.stdin
	outfile = sys.stdout
	
	if options.inputfile: 
		datain = open(options.inputfile,"r")
		filename = os.path.splitext(options.inputfile)[0]
	if options.inputfile and not options.outputfile:
		outfile = open("%s_hybcounts.txt" % filename,"w")
	if options.outputfile:
		outfile = open(options.outputfile,"w")
	###
	
	counter = defaultdict(int)
	
	for line in datain:
		Fld = line.strip().split("\t")
		combination = (Fld[-2], Fld[-1])
		counter[combination] += 1
	
	outfile.write("\tgene_A\tgene_B\tcounts\n")
	count = 1
	for i in sortbyvalue(counter):
		outfile.write("%s\t%s\t%s\t%s\n" % (count,i[0][0],i[0][1],counter[i[0]]))
		count += 1
	outfile.close()

if __name__ == "__main__":
	main()	
