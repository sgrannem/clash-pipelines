#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.2"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"

import sys
from pyCRAC.Parsers import GTF2
from pyCRAC.Methods import sortbyvalue
from optparse import OptionParser
from collections import defaultdict

def main():
	parser = OptionParser()
	parser.add_option("-f",dest="filename",help="the name of your .hyb input file",default=None)
	parser.add_option("--gtf",dest="gtf",help="the name of your gtf annotation file",default=None)
	parser.add_option("-o",dest="outputfile",help="the name of your count results output file. Default is the terminal",default=None)
	(options, args) = parser.parse_args()
	inputfile = sys.stdin
	
	gtf = GTF2.Parse_GTF()
	gtf.read_GTF(options.gtf)
	
	counter = defaultdict(int)
	
	if options.filename:
		inputfile = open(options.filename,"r")
	outputfile = sys.stdout
	if options.outputfile:
		outputfile=open(options.outputfile,"w")
	if not options.gtf:
		parser.error("No gtf annotation file was provided!\n")
		
	for line in inputfile:
		linesplit = line.strip().split("\t")
		first,second = linesplit[-2:]
		# only grab the first one if two gene names are provided.
		first = first.split(",")[0]
		second = second.split(",")[0]
		try:
			firstannotation = list(gtf.annotations(first))[0]
		except TypeError:
			continue
		try:
			secondannotation = list(gtf.annotations(second))[0]
		except TypeError:
			continue
		combination = "%s-%s" % (firstannotation,secondannotation)
		counter[combination] += 1
	
	for name,value in sortbyvalue(counter):
		outputfile.write("%s\t%s\n" % (name,value))
	outputfile.close()
	
if __name__ == "__main__":
	main()