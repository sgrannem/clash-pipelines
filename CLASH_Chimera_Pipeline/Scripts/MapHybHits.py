#!/usr/bin/python

__author__		= "Sander Granneman"
__copyright__	= "Copyright 2019"
__version__		= "0.0.5"
__credits__		= ["Sander Granneman"]
__maintainer__	= "Sander Granneman"
__email__		= "sgrannem@staffmail.ed.ac.uk"
__status__		= "beta"


import sys
from optparse import *
from collections import defaultdict
from pyCRAC.Methods import numpy_overlap, getfilename, reverse_strand
from pyCRAC.Parsers import GTF2

def main():		   
	parser = OptionParser(usage="Finds the gene names that map to your hybrids\n%prog [options] -f filename -o outfile", version="%s" % __version__)
	files = OptionGroup(parser, "File input options")
	files.add_option("-f", "--input_file", dest="inputfile",help="Provide the path and name of the .ua.hyb file.", metavar="FILE",default=None)
	files.add_option("-o","--output_file",dest="outputfile",help="Provide a name for an output file. By default the program writes to the standard output.",default=None)
	files.add_option("-r","--ranges",dest="ranges",type=int,help="To set the length of the UTRs (or flanking regions). Default is 0",default=0)
	files.add_option("--gtf",dest="gtf",help="Provide the name of your GTF annotation file.", metavar="mygtf.gtf")
	parser.add_option_group(files)
	(options, args) = parser.parse_args()
	
	if not options.gtf:
		parser.error("you forgot to include the path to your gtf annotation file. Please use the --gtf flag\n")

	### setting the input and output streams
	datain	= sys.stdin
	dataout = sys.stdout
	if options.inputfile: 
		datain = open(options.inputfile,"r")
	if options.outputfile:
		dataout = open(options.outputfile,"w")
	###
		
	### Making GTF2 parser object and parsing GTF annotation file ###
	gtf = GTF2.Parse_GTF()
	gtf.read_GTF(options.gtf,transcripts=False,ranges=options.ranges)
	###
	
	list_of_tuples = defaultdict(list)
	
	for line in datain:
		Fld = line.strip().split("\t")
		
		### Processing the first coordinates
		strand = str()
		chromosome = Fld[3].split("|")[0] 		# to make it compatible with Jai's hyb scripts
		start = int()
		end = int()
		first = int(Fld[6])
		second = int(Fld[7])
		if first < second: 	# strand should then be '+'
			strand = "+"
			start = first
			end = second
		else:
			strand = "-"
			start = second
			end = first
					
		sense_hits = list()
		anti_sense_hits = list()
		
		try:
			if (chromosome,strand) not in list_of_tuples:
				list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
			if len(list_of_tuples[(chromosome,strand)]) > 0:
				sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
		except AssertionError:
			pass
			
		if sense_hits:
			resultstring = ",".join(sense_hits)
		if not sense_hits:  ### Looking for anti-sense hits to gene features, including intergenic regions.
			try:
				strand = reverse_strand(strand)
				if (chromosome,strand) not in list_of_tuples:
					list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
				if len(list_of_tuples[(chromosome,strand)]) > 0:
					anti_sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
			except AssertionError:
				pass
		if anti_sense_hits:
			anti_sense_hits = ["as_%s" % i for i in anti_sense_hits]
			resultstring = ",".join(anti_sense_hits)
		if not sense_hits and not anti_sense_hits:
			sys.stdout.write("Can't find hits for %s %s %s %s" % (chromosome,start,end,strand))
			resultstring = "."
			
		Fld.append(resultstring)
		###
		
		### Processing the second coordinates

		strand = str()
		chromosome = Fld[9].split("|")[0] 		# to make it compatible with Jai's hyb scripts
		start = int()
		end = int()
		first = int(Fld[12])
		second = int(Fld[13])
		if first < second: 	# strand should then be '+'
			strand = "+"
			start = first
			end = second
		else:
			strand = "-"
			start = second
			end = first
					
		sense_hits = list()
		anti_sense_hits = list()
		
		try:
			if (chromosome,strand) not in list_of_tuples:
				list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
			if len(list_of_tuples[(chromosome,strand)]) > 0:
				sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
		except AssertionError:
			pass
			
		if sense_hits:
			resultstring = ",".join(sense_hits)
		if not sense_hits:  ### Looking for anti-sense hits to gene features, including intergenic regions.
			try:
				strand = reverse_strand(strand)
				if (chromosome,strand) not in list_of_tuples:
					list_of_tuples[(chromosome,strand)] = gtf.chromosomeGeneCoordIterator(chromosome,numpy=True,strand=strand)
				if len(list_of_tuples[(chromosome,strand)]) > 0:
					anti_sense_hits = numpy_overlap(list_of_tuples[(chromosome,strand)],start,end-1)
			except AssertionError:
				pass
		if anti_sense_hits:
			anti_sense_hits = ["as_%s" % i for i in anti_sense_hits]
			resultstring = ",".join(anti_sense_hits)
		if not sense_hits and not anti_sense_hits:
			sys.stdout.write("Can't find hits for %s %s %s %s" % (chromosome,start,end,strand))
			resultstring = "."
		
		Fld.append(resultstring)
		###

		dataout.write("%s\n" % "\t".join(Fld))

if __name__ == "__main__":
	main()

