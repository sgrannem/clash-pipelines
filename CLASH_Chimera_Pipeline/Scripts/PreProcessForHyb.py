#!/usr/bin/python

from __future__ import division
from ruffus import *
from ruffus.cmdline import MESSAGE
from collections import defaultdict
import ruffus.cmdline as cmdline
import subprocess
import platform
import re
import os
import argparse
import sys
import time

DEFAULTADAPT = "NAGATCGGAAGAGCACACG"

parser = cmdline.get_argparse(description="CRAC pipeline for processing paired-end CRAC data")
parser.add_argument("-f",dest="forwardreads",help="the path to your fastq files with forward reads.",metavar="data_1_1.fastq data_2_1.fastq ...",nargs="*",default=None)
parser.add_argument("-r",dest="reversereads",help="the path to your fastq files with reverse reads.",metavar="data_1_2.fastq data_2_2.fastq ...",nargs="*",default=None)
parser.add_argument("-g","--gtf",dest="gtf",help="the path to your gtf annotation file",metavar="yeast.gtf",default=None)
parser.add_argument("--db",dest="db",help="provide the path to the directory containing the bowtie and blast databases",default=None)
parser.add_argument("--join",dest="join",action="store_true",help="to join the paired reads that were not extended by flash. The reverse read will be reverse-complemented first",default=False)
parser.add_argument("--name",dest="name",help="provide a single word describing the run. Default is 'analysis' with a time stamp",default="analysis_%s" % time.strftime("%d%m%Y"))
parser.add_argument("--readlength",dest="readlength",type=int,help="provide the read length. Default is 75",default=75)
parser.add_argument("--truseq",dest="truseq",action="store_true",help="add this flag if your library was prepared using TruSeq kits. NOTE! This requires Flexbar version 3.4.0 or later! Default is False",default=False)
parser.add_argument("-b","--barcodes",dest="barcodes",help="the path to the file containing the list of barcodes. If you do not provide a barcode file, the demultiplexing step will be skipped",metavar="barcodes.txt",default=None)
parser.add_argument("-a","--adapter",dest="adapter",help="provide your 3' adapter sequence for trimming the reads using flexbar. If you do not provide an adapter sequence, the trimming step will be skipped",default=None)
parser.add_argument("-a2","--adapter2",dest="adapter2",help="use this option to trim any barcode/adapter sequences from the reverse read. Important when not using TrueSeq adapters and processing CRAC data! Default=None",default=None)
parser.add_argument("-m","--mismatches",dest="mismatches",type=int,help="indicate how many mismatches you allow for demultiplexing. Default is 1",default=1)
parser.add_argument("-p","--processors",dest="processors",type=int,help="indicate how many processors you want to use for analyses. Default is 8",default=8)
args = parser.parse_args()

if not args.forwardreads:
	parser.error("you forgot to include the path to your forward reads. Please use the -f flag\n")
elif not args.reversereads:
	parser.error("you forgot to include the path to your reverse reads. Please use the -r flag\n")
else:
	assert len(args.forwardreads) == len(args.reversereads), "ERROR! number of forward and reverse input files is not the same!\n"
if not args.gtf:
	parser.error("you forgot to include the path to your gtf annotation file. Please use the --gtf flag\n")
if not args.db:
	parser.error("you forgot to include the path to the folder that contains your bowtie and blast databases. Please use the --db flag\n")
totalnumberofsamples = len(args.forwardreads)
if args.barcodes:
	totalnumberofsamples = len(open(args.barcodes,"r").readlines())

def getFileBaseName(filename):
	""" removes path and extension from file name """
	return os.path.splitext(os.path.basename(filename))[0]

def getBarcodeInfo(barcodefile):
	return ["%s" % "_".join(line.strip().split()) for line in open(barcodefile,"r").readlines()]

def runFlexBar(inputfiles,outputfiles):
	""" runs Flexbar on the data to remove the adapter sequence from the forward reads """
	if args.adapter and not args.adapter2:
		cmd = "flexbar -r '%s' -p '%s' -qf i1.8 -n 10 -ao 7 --adapters '%s' --output-reads '%s' --output-reads2 '%s' -qt 30 -ap ON" % (inputfiles[0],inputfiles[1],args.adapter,outputfiles[0],outputfiles[1])
	elif args.adapter and args.adapter2:
		cmd = "flexbar -r '%s' -p '%s' -qf i1.8 -n 10 -ao 7 --adapters '%s' --adapters2 '%s' --output-reads '%s' --output-reads2 '%s' -qt 30 -ap ON" % (inputfiles[0],inputfiles[1],args.adapter,args.adapter2,outputfiles[0],outputfiles[1])
	elif args.truseq:
		cmd = "flexbar -r '%s' -p '%s' -qf i1.8 -n 10 -ao 7 -aa TruSeq --output-reads '%s' --output-reads2 '%s' -qt 30 -ap ON" % (inputfiles[0],inputfiles[1],outputfiles[0],outputfiles[1])
	else:
		cmd = "flexbar -r '%s' -p '%s' -qf i1.8 -n 10 -ao 7 --output-reads '%s' --output-reads2 '%s' -qt 30 -ap ON" % (inputfiles[0],inputfiles[1],outputfiles[0],outputfiles[1])
	logger.info(cmd)
	os.system(cmd)

def demultiplexSamples(inputfiles,outputfiles):
	""" demultiplexes all the samples """
	os.chdir(os.path.join(root_dir,"demultiplexed"))
	cmd = "pyBarcodeFilter.py -f '%s' -r '%s' -b '%s' -m '%s'" % (inputfiles[0],inputfiles[1],os.path.join(home_dir,args.barcodes),args.mismatches)
	logger.info(cmd)
	os.system(cmd)
	os.chdir(root_dir)

def runFlash(inputfiles,outputfiles):
	""" runs flash on the fastq files to find overlapping reads in the paired-end data.
	Produces a number of output files:
	data.extendedFrags.fastq (merged paired reads)
	data.notCombined_1.fastq (not combined forward)
	data.notCombined_2.fastq (not combined reverse) """
	outputdir = os.path.split(outputfiles[0])[0]						# directory
	outputprefix = os.path.split(outputfiles[0].split(".")[0])[-1]		# prefix
	cmd = "flash -O -t %s -m 5 -q -M %s -d %s -o %s %s %s" % (mincpus,args.readlength,outputdir,outputprefix,inputfiles[0],inputfiles[1])
	logger.info(cmd)
	os.system(cmd)

def joinPairedReads(inputfiles,outputfile):
	""" joins the not combined reads into one fastq file using pyFastJoiner.
	The reverse reads are reverse-complemented before joining and the qualities of the
	reverse read are reversed """
	cmd = "pyFastqJoiner.py -f %s %s --reversecomplement -o %s" % (inputfiles[1],inputfiles[2],outputfile)
	logger.info(cmd)
	os.system(cmd)

def mergeFastqFiles(inputfiles,outputfile):
	""" merges the extendedFrags reads and the paired reads joined by pyFastqJoiner.py """
	cmd = "cat %s %s > %s" % (inputfiles[0],inputfiles[1],outputfile)
	logger.info(cmd)
	os.system(cmd)

def runHybPipeline(inputfile,outputfiles):
	""" runs the hyb pipeline from the Tollervey lab """
	os.chdir(os.path.join(root_dir,"hybanalysis"))
	inputfile = os.path.abspath(inputfile)
	cmd = "hyb detect align=bowtie2 qc=none gmax=4 type=all hmax=10 in=%s anti=1 db=%s" % (inputfile,args.db)
	logger.info(cmd)
	os.system(cmd)
	os.chdir(root_dir)

def mapHybHits(inputfile,outputfile):
	""" maps gene features to the hybrid files and then splits them into inter-
	and intra-molecular interactions """
	cmd = "MapHybHits.py -f %s -o %s --gtf %s" % (inputfile,outputfile,args.gtf)
	logger.info(cmd)
	os.system(cmd)

def separateHybHits(inputfile,outputfiles):
	""" maps gene features to the hybrid files and then splits them into inter-
	and intra-molecular interactions """
	cmd = "SeparateHybrids.py -f %s -d 500" % (inputfile)
	logger.info(cmd)
	os.system(cmd)

def countHybHits(inputfiles,outputfiles):
	""" maps gene features to the hybrid files and then splits them into inter-
	and intra-molecular interactions """
	assert len(inputfiles) == len(outputfiles), "uneven number of input and output files detected!! Please correct!\n"
	for i in range(len(inputfiles)):
		cmd = "CountHybridCombinations.py -f %s -o %s" % (inputfiles[i],outputfiles[i])
		logger.info(cmd)
		os.system(cmd)

### setting the number of cpus
maxcpus = int(args.processors)
mincpus = int(round(maxcpus/totalnumberofsamples))
###

### setting up the database path
databasepath,databasename = os.path.split(os.path.abspath(args.db))
###

### checking if dependencies are installed:
#dependencies = ["flash","flexbar","pyFastqDuplicateRemover.py","CountHybridCombinations.py","SeparateHybrids.py","MapHybHits.py","hyb"]
#
#for i in dependencies:
#	cmd = "where" if platform.system() == "Windows" else "which"
#	try:
#		subprocess.call([cmd, "%s"])
#	except:
#		sys.stderr.write("%s is not installed (properly) on your system. Please (re)install" % i)
#		exit()
###

### start of pipeline

# setting the main working directory
home_dir = os.getcwd()
root_dir = "%s/%s" % (home_dir,args.name)
try:
	os.mkdir(root_dir)
except OSError:
	pass

### setting up logging
if not args.log_file:
	logfile = "%s/%s" % (root_dir,"log_file.txt")
else:
	logfile = "%s/%s" % (root_dir,args.log_file)
logger, logger_mutex = cmdline.setup_logging ("PreProcessForHyb",logfile,10)

### setting the starting files

assert len(args.forwardreads) == len(args.reversereads), "ERROR! number of forward and reverse input files is not the same!\n"

args.forwardreads = [os.path.abspath(i) for i in args.forwardreads]
args.reversereads = [os.path.abspath(i) for i in args.reversereads]
startingfiles = list(zip(args.forwardreads,args.reversereads))
print(startingfiles)

### checking if the correct pyBarcodeFilter version is installed. Need version 3.0 or higher!

result = subprocess.Popen(['pyBarcodeFilter.py', '--version'], stdout=subprocess.PIPE)
out,err = result.communicate()
if float(out) < 3.0:
	sys.stderr.write("To run this script you need to have pyBarcodeFilter version 3.0 or later installed. Please install the latest version of pyCRAC\n")
	exit()

### starting the pipeline

pipeline = Pipeline(name="PreProcessForHyb")

if args.barcodes and args.adapter:			# if trimming and demultiplexing is required
	sys.stdout.write("Trimming and demultiplexing the data...\n")
	pipeline.transform(task_func = runFlexBar,
			input  = startingfiles,
			filter = formatter("^.+/([^/]+)_1.(san)?fastq$","^.+/([^/]+)_2.(san)?fastq$"),
			output = ["%s/flexbar_trimmed/{1[0]}_trimmed_1.fastq" % root_dir,"%s/flexbar_trimmed/{1[1]}_trimmed_2.fastq" % root_dir],
			).follows(pipeline.mkdir(os.path.join(root_dir,"flexbar_trimmed")))

	pipeline.subdivide(task_func = demultiplexSamples,
			input  = runFlexBar,
			filter = formatter("^.+/([^/]+)_1.fastq","^.+/([^/]+)_2.fastq"),
			output = [["%s/demultiplexed/{1[0]}_1_%s.fastq" % (root_dir,barcodestring),"%s/demultiplexed/{1[1]}_2_%s.fastq" % (root_dir,barcodestring)] for barcodestring in getBarcodeInfo(args.barcodes)]
			).follows(pipeline.mkdir(os.path.join(root_dir,"demultiplexed")),runFlexBar)

	pipeline.transform(task_func = runFlash,
			input  = demultiplexSamples,
			filter = formatter("^.+/([^/]+)_trimmed_1_(?P<BARCODE>.*).fastq","^.+/([^/]+)_trimmed_2_(?P<BARCODE>.*).fastq"),
			output = ["%s/flashoutput/{1[0]}_trimmed_{BARCODE[0]}.extendedFrags.fastq" % root_dir,"%s/flashoutput/{1[0]}_trimmed_{BARCODE[0]}.notCombined_1.fastq" % root_dir,"%s/flashoutput/{1[1]}_trimmed_{BARCODE[1]}.notCombined_2.fastq" % root_dir]
			).follows(pipeline.mkdir(os.path.join(root_dir,"flashoutput")),demultiplexSamples)

elif args.adapter:							# if only trimming is required
	sys.stdout.write("Only trimming the data...\n")
	pipeline.transform(task_func = runFlexBar,
			input  = startingfiles,
			filter = formatter("^.+/([^/]+)_1.(san)?fastq$","^.+/([^/]+)_2.(san)?fastq$"),
			output = ["%s/flexbar_trimmed/{1[0]}_trimmed_1.fastq" % root_dir,"%s/flexbar_trimmed/{1[1]}_trimmed_2.fastq" % root_dir],
			).follows(pipeline.mkdir(os.path.join(root_dir,"flexbar_trimmed")))

	pipeline.transform(task_func = runFlash,
			input  = startingfiles,
			filter = formatter("^.+/([^/]+)_1.fastq","^.+/([^/]+)_2.fastq"),
			output = ["%s/flashoutput/{1[0]}_{1[1]}.extendedFrags.fastq" % root_dir,"%s/flashoutput/{1[0]}_{1[1]}.notCombined_1.fastq" % root_dir,"%s/flashoutput/{1[0]}_{1[1]}.notCombined_2.fastq" % root_dir]
			).follows(pipeline.mkdir(os.path.join(root_dir,"flashoutput")),runFlexBar)
else:										# if the files have already been trimmed and demultiplexed
	sys.stdout.write("No trimming or demultiplexing was requested...\n")
	pipeline.transform(task_func = runFlash,
			input  = startingfiles,
			filter = formatter("^.+/([^/]+)_1(.*).fastq","^.+/([^/]+)_2(.*).fastq"),
			output = ["%s/flashoutput/{1[0]}_{2[0]}.extendedFrags.fastq" % root_dir,"%s/flashoutput/{1[0]}_{2[0]}.notCombined_1.fastq" % root_dir,"%s/flashoutput/{1[0]}_{2[0]}.notCombined_2.fastq" % root_dir]
			).follows(pipeline.mkdir(os.path.join(root_dir,"flashoutput")))

if args.join:
	pipeline.transform(task_func = joinPairedReads,
			input  = runFlash,
			filter = formatter("(.*).extendedFrags.fastq","(.*).notCombined_1.fastq","(.*).notCombined_2.fastq"),
			output = "{1[0]}.notCombined_merged.fastq"
			)

	pipeline.transform(task_func = mergeFastqFiles,
			input  = joinPairedReads,
			filter = formatter("(.*).notCombined_merged.fastq"),
			output = "{1[0]}.joined_and_extended_merged.fastq",
			add_inputs = add_inputs("{1[0]}.extendedFrags.fastq")
			)

else:
	pipeline.transform(task_func = mergeFastqFiles,
			input  = runFlash,
			filter = formatter("(.*).extendedFrags.fastq","(.*).notCombined_1.fastq","(.*).notCombined_2.fastq"),
			output = "{1[0]}.joined_and_extended_merged.fastq",
			)

pipeline.transform(task_func = runHybPipeline,
		input  = mergeFastqFiles,
		filter = formatter(),
		output = "%s/hybanalysis/{basename[0]}_comp_%s_hybrids_ua.hyb" % (root_dir,databasename)
		).follows(pipeline.mkdir(os.path.join(root_dir,"hybanalysis")))

pipeline.transform(task_func = mapHybHits,
		input  = runHybPipeline,
		filter = formatter(),
		output = "%s/mappedhybreads/{basename[0]}_gene_names.hyb" % root_dir
		).follows(pipeline.mkdir(os.path.join(root_dir,"mappedhybreads")),runHybPipeline)

pipeline.transform(task_func = separateHybHits,
		input  = mapHybHits,
		filter = formatter(),
		output = ["{path[0]}/{basename[0]}_intermolecular.hyb","{path[0]}/{basename[0]}_intramolecular.hyb"]
		)

pipeline.transform(task_func = countHybHits,
		input  = separateHybHits,
		filter = formatter("(.*).hyb","(.*).hyb"),
		output = ["{1[0]}_hybcounts.txt","{1[1]}_hybcounts.txt"]
		)

pipeline_run(multiprocess=args.processors,verbose=5)
