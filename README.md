# CLASH sequencing data processing pipeline:

## Contents

- [Overview](#overview)
- [Repo Contents](#repo-contents)
- [System Requirements](#system-requirements)
- [Installation Guide](#installation-guide)
- [Results](#results)
- [License](./LICENSE.txt)
- [Citation](#citation)

## Overview
This is a repository containing the scripts for the single-end (SE) and paired-end (PE) RNA sequencing pipelines.
This pipeline is routinely used by our lab to process single-and paired-end CRAC datasets as well as RNA-sequencing datasets.
The pipeline does the following steps:

1) Uses pyBarcodeFilter (pyCRAC package) to demultiplex the data using in-read barcode sequences (optional step)
2) Trims the adapters (optional step)
3) Merges the reads together so that they can be processed by the hyb pipeline
4) Maps the halves of the chimeras to individual genes
5) Separates intramolecular from intermolecular chimeras

## Repo Contents

- [CLASH_Chimera_Pipeline](./CLASH_Chimera_Pipeline): All the code for processing the fastq files
- [adapter_sequences](./adapter_sequences): Adapter sequences that we routinely use for CRAC/CLASH analyses. This is required for trimming the reads.
- [test_data](./test_data): Folder with raw fastq files. These are used for testing the entire pipeline.
- [test_code](./test_code): Folder with a jupyter notebook that you can run from within the repository to test the pipeline on you computer.
- [genome_files](./genome_files): Folder with all the Staphyloccoccus areus genome files that we use for the chimera analyses.

The notebook in the test_code directory provides examples of how to run the code so that you can use it for your own data.

## System Requirements

### Hardware Requirements

RAM: 16+ GB  
CPU: At least four cores are recommended.

The runtimes vary considerably, depending on how large the sequencing data files are.
But a typical run generally takes several hours to complete.

### Software requirements

Linux: Ubuntu 18.04 and higher.

The pipeline should also work on OSX machines, but the latest versions of novoalign
have only been compiled for linux.

Other dependencies that need to be installed.

 - Python 3.6+ or higher 
 - ruffus 2.6 or higher (http://www.ruffus.org.uk)
 - pyCRAC 1.5.1 or higher (https://git.ecdf.ed.ac.uk/sgrannem/pycrac)
 - flexbar 3.5 or higher (https://github.com/seqan/flexbar)
 - novoalign 2.0 or higher (http://www.novocraft.com/products/novoalign/)
 - FLASH >=1.2.11 (https://ccb.jhu.edu/software/FLASH/)
 - hyb >0.0 (https://github.com/gkudla/hyb)
 - bowtie2 (http://bowtie-bio.sourceforge.net/bowtie2/index.shtml)
 - blast+ (https://blast.ncbi.nlm.nih.gov/Blast.cgi?CMD=Web&PAGE_TYPE=BlastDocs&DOC_TYPE=Download)

The python setup.py in this package will automatically install any Python dependencies.

## Installation Guide

Go to the directory containing the repostitory and type:

```
sudo python setup.py install
```

This will install the Python scripts that underpin the pipeline.

## Results

Each script has a detailed help menu. To access this type the following:

```
PreProcessForHyb.py  -h
```

To run the code on the test data, please have a look at the jupyter notebook in the test_code directory.
The results of these test runds can be found in the test_code/test_run directory.

## Citation

For usage of the package and associated manuscript, please cite the following papers:

1. Iosub, I. A. et al. Hfq CLASH uncovers sRNA-target interaction networks linked to nutrient availability adaptation. Elife 9, 1–33 (2020).
2. McKellar, S. W. et al. RNase III CLASH in MRSA uncovers sRNA regulatory networks coupling metabolism to toxin expression. submitted (2021).
